/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include "Kwadrat.h"
#include "Punkt.h"

Kwadrat::Kwadrat(Punkt p, double tmpbok)
{
	wierzcholek = p;
	bok = tmpbok;
}

void Kwadrat::wypisz()
{
	std::cout << "Kwadrat o boku " << bok << " i lewym gornym wierzcholku o wspolrzednych ";
	wierzcholek.wypisz();
	std::cout << std::endl;
}