/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#ifndef FIG_H
#define FIG_H

class Fig
{
public:
    virtual void wypisz() = 0;
    virtual ~Fig(){}
};

#endif