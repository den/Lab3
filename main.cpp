/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#include "Fig.h"
#include "Kolo.h"
#include "Kwadrat.h"
#include "Trojkat.h"

const int NFIG=3;

int main()
{
   Fig* scena[NFIG];
   scena[0] = new Kolo(Punkt(0, 0), 4);
   scena[1] = new Kwadrat( Punkt( 0, 0 ), 5 );
   scena[2] = new Trojkat(Punkt(0, 0), Punkt(4, 0), Punkt(0, 5));
   for(int i = 0; i < NFIG; i++ )
      scena[i]->wypisz();    

   // Po mojemu to to jest ta magiczna linijka, bez kt�rej program zadzia�a, ale musi by�
   for(int i = 0; i < NFIG; i++) delete scena[i];

   return 0;
}