/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include "Trojkat.h"
#include "Punkt.h"

Trojkat::Trojkat(Punkt p1, Punkt p2, Punkt p3)
{
	wierzcholki[0] = p1;
	wierzcholki[1] = p2;
	wierzcholki[2] = p3;
}

void Trojkat::wypisz()
{
	std::cout << "Trojkat o wierzcholkach ";
	for(int i = 0; i < 3; i++)
	{
		wierzcholki[i].wypisz();
		if(i == 1) std::cout << " i ";
		else std::cout << ", ";
	}
	std::cout << std::endl;
}