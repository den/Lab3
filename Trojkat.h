/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#ifndef TROJKAT_H
#define TROJKAT_H

#include "Punkt.h"
#include "Fig.h"

class Trojkat: public Fig
{
	protected:
	   Punkt wierzcholki[3];
	public:
		void wypisz();

		/**
		 * Konstruktor
		 * @param {Punkt} p1
		 * @param {Punkt} p2
		 * @param {Punkt} p3
		 */
		Trojkat(Punkt p1, Punkt p2, Punkt p3);
};

#endif